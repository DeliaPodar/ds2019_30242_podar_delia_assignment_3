
class PaginationComponent {
  
  constructor(limit) {
    if (limit === undefined || limit.toString().length === 0) {
      throw new Error('limit eh um parametro obrigatorio')
    }

    this.limit = limit
    this.offset = 0
  }

  goToFirstPage() {
    return new PaginationComponent(this.limit)
  }

  goToPreviousPage() {
    if(this.offset === 0)
      return this
    
    const p = new PaginationComponent(this.limit)
    p.offset = this.offset - p.limit
    return p
  }

  goToNextPage(rows) {
    if(rows === undefined) 
      throw new Error('o parametro rows eh obrigatorio para goToNextPage()')

    if(rows === (this.offset + this.limit))
      return this
    
    const p = new PaginationComponent(this.limit)
    p.offset = this.offset
    p.offset = p.offset + p.limit
    return p
  }

  getTotalPages(rows) {
    if(rows === undefined) 
      throw new Error('o parametro rows eh obrigatorio para getTotalPages()')
    
    let pages = Math.floor(rows / this.limit)

    if(rows % this.limit !== 0)
      pages = pages + 1

    return pages
  }

  getCurrentPage() {
    return Math.floor((this.offset + this.limit) / this.limit)
  }

  goToLastPage(rows) {
    if(rows === undefined)
      throw new Error('o parametro rows eh obrigatorio para goToLastPage()')

    const p = new PaginationComponent(this.limit)
    
    if(rows % p.limit !== 0) {
      const resto = rows % p.limit
      p.offset = (rows - resto)
    } else {
      p.offset = rows - p.limit
    }

    return p
  }

  getCurrentState(rows) {
    let state = []
    const total = this.getTotalPages(rows)

    let offset = 0
    for(let i = 0; i < total; i++) {
      state.push({ 
        page: (i + 1), 
        offset: offset, 
        currentPage: (i + 1) === this.getCurrentPage()
      })

      offset = offset + this.limit
    }

    return state
  }

  isPageFirstVisited() {
    return this.getCurrentPage() === 1
  }

  isPageLastVisited(rows) {
    const max = this.getTotalPages(rows)
    
    return this.getCurrentPage() === max
  }

  isPageBetweenFirstLastVisited(rows) {
    const max = this.getTotalPages(rows)

    return this.getCurrentPage() > 1 && this.getCurrentPage() < max
  }

}

export default PaginationComponent
