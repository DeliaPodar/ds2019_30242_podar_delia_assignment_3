import React, { Component } from 'react'
import PropTypes from 'prop-types'

import { Table, Menu, Icon } from 'semantic-ui-react'

import { colunas } from './EspecialidadesSearch'
import EspecialidadeModal from './EspecialidadeModal'
import EspecialidadeModalDelete from './EspecialidadeModalDelete'

class EspecialidadesTable extends Component {

    static propTypes = {
        especialidades: PropTypes.array.isRequired,
        rows: PropTypes.number.isRequired,
        especialidadesSearch: PropTypes.object.isRequired,
        atualizarEspecialidades: PropTypes.func.isRequired
    }

    constructor(props) {
		super(props);

		this.state = { };
    }

    sort = (col) => {
        this.props.especialidadesSearch.changeSort(col)
        this.props.atualizarEspecialidades()
    }

    pageChange = (page) => {
        this.props.especialidadesSearch.paginationState.currentPage = page;
        this.props.atualizarEspecialidades();
    }

    goFirstPage = () => {
        this.pageChange(0)
    }

    goLastPage = (numPages) => {
        this.pageChange((numPages - 1) * this.props.especialidadesSearch.paginationState.rowsPerPage)
    }

    renderSortIcon = (coluna) => {
        if(coluna === this.props.especialidadesSearch.tableState.sortColumn) {
            if(this.props.especialidadesSearch.tableState.sortDirection === 'asc') {
                return <Icon name='sort content ascending' size='large' />
            } else {
                return <Icon name='sort content descending' size='large' />
            }
        }

        return <Icon name='sort' size='large' />
    }

    render() {
        let especialidades = this.props.especialidades;

        if (!especialidades) {
            console.log('Carregando Especialidades...');
            return (<div>Carregando Especialidades...</div>);
        }

        let tableRows = especialidades.map((especialidade) => 
            <Table.Row key={especialidade.id}>
                <Table.Cell>{especialidade.nome_especialidade}</Table.Cell>
                <Table.Cell width={3}>{especialidade.created_at}</Table.Cell>
                <Table.Cell width={3}>{especialidade.updated_at}</Table.Cell>
                <Table.Cell width={2}>
                    <EspecialidadeModal
                        headerTitle='Edição de Especialidade'
                        icon='edit'
                        buttonSubmitTitle='Salvar'
                        buttonColor='blue'
                        atualizarEspecialidades={this.props.atualizarEspecialidades}
                        idEspecialidade={especialidade.id}
                        onEspecialidadeUpdated={this.props.onEspecialidadeUpdated} />

                    <EspecialidadeModalDelete
                        headerTitle='Confirmação de Remoção de Especialidade'
                        icon='remove'
                        buttonColor='red'
                        especialidade={especialidade}
                        atualizarEspecialidades={this.props.atualizarEspecialidades}
                        onEspecialidadeDeleted={this.props.onEspecialidadeDeleted} />
                </Table.Cell>
            </Table.Row>
        );

        let numPages = this.props.especialidadesSearch.getNumberOfPagesForPagination(this.props.rows)
        
        let paginationItens = [];

        for(let index = 0; index < numPages; index++) {
            paginationItens.push(
                <Menu.Item 
                    key={index} 
                    active={index === this.props.especialidadesSearch.paginationState.currentPage} 
                    onClick={() => this.pageChange(index * this.props.especialidadesSearch.paginationState.rowsPerPage)} 
                    as='a'>{index+1}</Menu.Item>
            );
        }

        return (
            <Table striped celled compact>
                <Table.Header>
                    <Table.Row>
                        <Table.HeaderCell onClick={() => this.sort(colunas.NOME_ESPECIALIDADE)}>Nome {this.renderSortIcon(colunas.NOME_ESPECIALIDADE)}</Table.HeaderCell>
                        <Table.HeaderCell onClick={() => this.sort(colunas.CREATED_AT)}>Data de Criação {this.renderSortIcon(colunas.CREATED_AT)}</Table.HeaderCell>
                        <Table.HeaderCell onClick={() => this.sort(colunas.UPDATED_AT)}>Data de Atualização {this.renderSortIcon(colunas.UPDATED_AT)}</Table.HeaderCell>
                        <Table.HeaderCell></Table.HeaderCell>
                    </Table.Row>
                </Table.Header>
                <Table.Body>
                    {tableRows}
                </Table.Body>
                <Table.Footer>
                    <Table.Row>
                        <Table.HeaderCell colSpan='4'>
                        <Menu floated={true} pagination>
                            <Menu.Item as='a' icon 
                                onClick={() => this.goFirstPage()}>
                                <Icon name='angle double left' />
                            </Menu.Item>
                            <Menu.Item as='a' icon>
                                <Icon name='angle left' />
                            </Menu.Item>
                            {paginationItens}
                            <Menu.Item as='a' icon>
                                <Icon name='angle right' />
                            </Menu.Item>
                            <Menu.Item as='a' icon
                                onClick={() => this.goLastPage(numPages)}>
                                <Icon name='angle double right' />
                            </Menu.Item>
                        </Menu>
                        </Table.HeaderCell>
                    </Table.Row>
                </Table.Footer>
            </Table>
        );
    }

}

export default EspecialidadesTable;