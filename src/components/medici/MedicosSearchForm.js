import React from 'react'
import PropTypes from 'prop-types'

import { Dropdown } from 'semantic-ui-react'

const renderEstados = (props) => {
  const estados = []
  if(props.estados !== undefined) {
    props.estados.forEach(e => {
      estados.push(<option key={e.id} value={e.id}>{e.nome_estado}</option>)
    })
  }

  return estados
}

const MedicosSearchForm = (props) => {

  const searchEspecialidadeChanged = (event, data) => {
    props.handleInputChange(data.name, data.value)
  }

  const searchNomeMedicoChanged = (e) => {
    props.handleInputChange('searchNomeMedico', e.target.value)
  }

  const searchCpfChanged = (e) => {
    props.handleInputChange('searchCpf', e.target.value)
  }

  const searchEstadoCrmChanged = (e) => {
    props.handleInputChange('searchEstadoCrm', e.target.value)
  }

  return (
  <form className="search-form">
    <label className="search-form-label" style={{width: '180px'}}>Nome : </label>
    <input 
      type="text" 
      id="searchNomeMedico" name="searchNomeMedico" 
      placeholder='Nome do Médico' 
      className="search-form-input" style={{width: '300px'}}
      value={props.searchNomeMedico}
      onChange={searchNomeMedicoChanged} />
    
    <label className="search-form-label" style={{width: '180px'}}>CPF : </label>
    <input 
      type="text" 
      id="searchCpf" name="searchCpf" 
      placeholder='CPF do Médico' 
      className="search-form-input" style={{width: '300px'}}
      value={props.searchCpf}
      onChange={searchCpfChanged} />
    <br />

    <label className="search-form-label" style={{width: '180px'}}>Estado CRM : </label>
    <select
      name="searchEstadoCrm"
      className="search-form-input" style={{width: '300px'}}
      value={props.searchEstadoCrm}
      onChange={searchEstadoCrmChanged}>
      <option value="">Selecione o Estado</option>
      { renderEstados(props) }
    </select>

    <label className="search-form-label" style={{width: '180px'}}>Especialidade : </label>
    <Dropdown 
      name="searchEspecialidade"
      placeholder='Selecione a Especialidade' 
      search selection 
      className="search-form-input" style={{width: '300px'}}
      value={props.searchEspecialidade}
      onChange={searchEspecialidadeChanged}
      options={props.especialidades} />
  </form>
  )
}

MedicosSearchForm.propTypes = {
  handleInputChange: PropTypes.func.isRequired,
  medicosSearch: PropTypes.object.isRequired,
  especialidades: PropTypes.array.isRequired,
  estados: PropTypes.array.isRequired,
}

export default MedicosSearchForm