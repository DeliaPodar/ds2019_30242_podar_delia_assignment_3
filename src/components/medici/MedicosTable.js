import React, { Component } from 'react'
import PropTypes from 'prop-types'

import { 
  Table,
  Menu,
  Icon,
  Button,
  Modal
} from 'semantic-ui-react'

import MedicoAPI from '../../api/MedicoAPI'
import MedicoModal from './MedicoModal'
import { colunas } from './MedicosSearch'

class MedicosTable extends Component {

  static propTypes = {
    medicos: PropTypes.array.isRequired,
    rows: PropTypes.number.isRequired,
    atualizarTabela: PropTypes.func.isRequired,
    paginationChanged: PropTypes.func.isRequired
  }

  constructor(props) {
		super(props)

    this.state = {
      showModalEdicao: false,
      showModalRemocao: false,
      medico: {}
    }
  }

  componentDidMount() { }

  componentWillUnmount() { }

  goToFirstPage() {
    this.props.paginationChanged(this.props.pagination.goToFirstPage())
  }

  goToLastPage() {
    this.props.paginationChanged(this.props.pagination.goToLastPage(this.props.rows))
  }

  goToNextPage() {
    this.props.paginationChanged(this.props.pagination.goToNextPage(this.props.rows))
  }

  goToPreviousPage() {
    this.props.paginationChanged(this.props.pagination.goToPreviousPage(this.props.rows)) 
  }

  sort(column) {
    this.props.medicosSearch.changeSort(column)
    this.props.atualizarTabela()
  }

  showModalEdicao = (idMedico) => {
    MedicoAPI.getById(idMedico)
      .done((response) => {
        this.setState({ showModalEdicao: true, medico: response })
      })
      .fail(() => {})
  }

  showModalRemocao = (idMedico) => {
    MedicoAPI.getById(idMedico)
      .done((response) => {
        this.setState({ showModalRemocao: true, medico: response })
      })
      .fail(() => {})
  }

  hideFormEdicao = () => this.setState({ showModalEdicao: false, medico: {} }, () => this.props.atualizarTabela())
  
  hideFormRemocao = () => this.setState({ showModalRemocao: false, medico: {} }, () => this.props.atualizarTabela())

  removeMedico = () => {
    MedicoAPI.delete(this.state.medico.id)
      .done((response) => {
        this.setState({ showModalRemocao: false, medico: {} }, () => this.props.atualizarTabela())
      })
      .fail(() => {})
  }

  renderPaginationLinks = () => {
    let pages = []
    const totalPages = this.props.pagination.getTotalPages(this.props.rows)

    let offset = 0
    
    for(let i = 1; i <= totalPages; i++) {
      const currentOffset = offset
      
      pages.push((
        <Menu.Item key={i} as='a' icon onClick={() => this.props.offsetChanged(currentOffset)}>
          <span>{i}</span>
        </Menu.Item>
      ))

      offset = offset + this.props.pagination.limit

      if(i === 9) break // TODO: tenho que tratar quando for um numero maior de pages
    }

    return pages
  }

  renderSortIcon = (coluna) => {
    if(coluna === this.props.medicosSearch.sortColumn) {
        if(this.props.medicosSearch.sortDirection === 'asc') {
          return <Icon name='sort content ascending' size='large' />
        } else {
          return <Icon name='sort content descending' size='large' />
        }
    }

    return <Icon name='sort' size='large' />
  }

  render() {
    const tableRows = this.props.medicos.map((medico) => 
      <Table.Row key={medico.id}>
        <Table.Cell></Table.Cell>
        <Table.Cell>{medico.nome_medico}</Table.Cell>
        <Table.Cell>{medico.cpf}</Table.Cell>
        <Table.Cell>{medico.telefone}</Table.Cell>
        <Table.Cell>{medico.especialidade.nome_especialidade}</Table.Cell>
        <Table.Cell>
          <Button icon color='blue' onClick={() => this.showModalEdicao(medico.id)}>
            <Icon name='edit' />
          </Button>

          <Button icon color='red' onClick={() => this.showModalRemocao(medico.id)}>
            <Icon name='remove' />
          </Button>
        </Table.Cell>
      </Table.Row>
    )

    return (
      <div>
      <Table striped celled compact>
        <Table.Header>
          <Table.Row>
            <Table.HeaderCell width={2}>Foto</Table.HeaderCell>
            <Table.HeaderCell width={3} onClick={() => this.sort(colunas.NOME_MEDICO)}>Nome {this.renderSortIcon(colunas.NOME_MEDICO)}</Table.HeaderCell>
            <Table.HeaderCell width={2}>CPF</Table.HeaderCell>
            <Table.HeaderCell width={2}>Telefone</Table.HeaderCell>
            <Table.HeaderCell onClick={() => this.sort(colunas.ESPECIALIDADE)}>Especialidade {this.renderSortIcon(colunas.ESPECIALIDADE)}</Table.HeaderCell>
            <Table.HeaderCell width={2}></Table.HeaderCell>
          </Table.Row>
        </Table.Header>

        <Table.Body>
          { tableRows }
        </Table.Body>

        <Table.Footer>
          <Table.Row>
            <Table.HeaderCell colSpan='6'>
              <Menu floated={true} pagination>
                <Menu.Item as='a' icon onClick={() => this.goToFirstPage()}>
                  <Icon name='angle double left' />
                  <span style={{marginLeft: '5px'}}>First</span>
                </Menu.Item>
                <Menu.Item as='a' icon onClick={() => this.goToPreviousPage()}>
                  <Icon name='angle left' />
                  <span style={{marginLeft: '5px'}}>Previous</span>
                </Menu.Item>
                { this.renderPaginationLinks() }
                <Menu.Item as='a' icon onClick={() => this.goToNextPage()}>
                  <span style={{marginRight: '5px'}}>Next</span>
                  <Icon name='angle right' />
                </Menu.Item>
                <Menu.Item as='a' icon onClick={() => this.goToLastPage()}>
                  <span style={{marginRight: '5px'}}>Last</span>
                  <Icon name='angle double right' />
                </Menu.Item>
              </Menu>
            </Table.HeaderCell>
          </Table.Row>
        </Table.Footer>
      </Table>

      <MedicoModal 
        modalOpen={this.state.showModalEdicao}
        headerTitle='Editar Médico'
        buttonSubmitTitle='Salvar'
        buttonColor='blue'
        hideFormCadastro={this.hideFormEdicao}
        especialidades={this.props.especialidades}
        estados={this.props.estados}
        medico={this.state.medico} />

      <Modal
        open={this.state.showModalRemocao}
        dimmer='inverted'
        size='small'>
        <Modal.Header>Confirmação de Remoção do Médico</Modal.Header>
        <Modal.Content>
          <p>Você tem certeza que deseja remover o Médico de nome: 
          <strong>{this.state.medico.nome_medico}</strong>?</p>
        </Modal.Content>
        <Modal.Actions>
          <Button onClick={this.removeMedico} color='red'>Sim</Button>
          <Button onClick={this.hideFormRemocao} color='black'>Não</Button>
        </Modal.Actions>
      </Modal>

      </div>
    )
  }

}

export default MedicosTable