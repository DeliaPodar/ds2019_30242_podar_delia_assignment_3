import React from 'react';
import ReactDOM from 'react-dom';

import { BrowserRouter } from 'react-router-dom';

import 'jquery/dist/jquery.js';
import 'jquery-mask-plugin/dist/jquery.mask.js';

import 'semantic-ui-css/semantic.min.css';
import './index.css';

import App from './components/App';
import * as serviceWorker from './serviceWorker';

const mainApp = 
  <BrowserRouter>
    <App />
  </BrowserRouter>;

ReactDOM.render(mainApp, document.getElementById('root'));

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: http://bit.ly/CRA-PWA
serviceWorker.unregister();
